import flask
import requests
import json

from io import BytesIO
from PIL import Image

import img_processing
import helpers


app = flask.Flask(__name__)


@app.route("/")
def hello():
    return "App is running"


@app.route('/oauth', methods=['GET'])
def oauth():
    with open('config') as json_data:
        config = json.load(json_data)

    client_id = config['client_id']
    client_secret = config['client_secret']
    slack_oauth_url = config['urls']['slack_oauth']

    code = flask.request.args['code']

    if code:
        query = {
            'code'         : code,
            'client_id'    : client_id,
            'client_secret': client_secret
        }

        requests.get(slack_oauth_url, params=query)
        return 'yay'
    else:
        abort(400, 'Required parameter \"code\" missing.')


@app.route('/gen_image', methods=['GET'])
def gen_image():
    image_key = flask.request.args['img_key']
    caption_top = flask.request.args['caption_top']
    caption_bottom = flask.request.args['caption_bottom']

    font_file_path = 'anton.ttf'

    image = helpers.get_image_by_key(image_key)
    generated_image = img_processing.add_captions(caption_top, caption_bottom, font_file_path, image)
    image_bytes_io = helpers.image_to_bytes_io(generated_image)

    return flask.send_file(image_bytes_io, mimetype='image/png')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)
