from io import BytesIO
from PIL import Image


def get_image_by_key(image_key):
    image_path = 'img/' + image_key

    with open(image_path, 'rb') as bin_file:
        image_bytes = bin_file.read()

    image_file = BytesIO(image_bytes)

    return Image.open(image_file)


def image_to_bytes_io(image):
    image_file = BytesIO()
    image.save(image_file, format='PNG')
    image_file.seek(0)
    return image_file

