import io
from PIL import Image, ImageFont

import img_processing

img_path = 'img/cat'
gen_img_path = 'img/genned_img.png'
font_path = 'impact.ttf'

with open(img_path, 'rb') as bin_file:
    img_bytes = bin_file.read()

bg_image = Image.open(io.BytesIO(img_bytes))

genned_img = img_processing.add_captions('hello', 'bye', font_path, bg_image)
genned_img.save(gen_img_path)

