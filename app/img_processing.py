# # License
# 
# **The MIT License (MIT)**
# 
# Copyright &copy; 2017, Jace Browning
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# 
# ----------------------------------------------------------------------------
# 
# Some minor modifications by Robert Soultanaev. Original source code is hosted
# at https://github.com/jacebrowning/memegen as of the time this was written.
# 
# ----------------------------------------------------------------------------




import os

from PIL import Image as ImageFile, ImageFont, ImageDraw, ImageFilter


def add_captions(top, bottom, font_path, bg_image):
    """Add text to an image and save it."""

    bg_image = bg_image.convert('RGB')
    bg_image.format = 'PNG'

    # Draw image
    draw = ImageDraw.Draw(bg_image)

    max_font_size = int(bg_image.size[1] / 5)
    min_font_size_single_line = int(bg_image.size[1] / 12)
    max_text_len = bg_image.size[0] - 20

    if top:
        top_font_size, top = _optimize_font_size(
            font_path, top, max_font_size,
            min_font_size_single_line, max_text_len)

        top_font = ImageFont.truetype(font_path, top_font_size)
        top_text_size = draw.multiline_textsize(top, top_font)

        # Find top centered position for top text
        top_text_position_x = (bg_image.size[0] / 2) - (top_text_size[0] / 2)
        top_text_position_y = 0
        top_text_position = (top_text_position_x, top_text_position_y)

        _draw_outlined_text(draw, top_text_position,
                            top, top_font, top_font_size)

    if bottom:
        bottom_font_size, bottom = _optimize_font_size(
            font_path, bottom, max_font_size,
            min_font_size_single_line, max_text_len)

        bottom_font = ImageFont.truetype(font_path, bottom_font_size)
        bottom_text_size = draw.multiline_textsize(bottom, bottom_font)


        # Find bottom centered position for bottom text
        bottom_text_size_x = (bg_image.size[0] / 2) - (bottom_text_size[0] / 2)
        bottom_text_size_y = bg_image.size[1] - bottom_text_size[1] * (7 / 6)
        bottom_text_position = (bottom_text_size_x, bottom_text_size_y)

        _draw_outlined_text(draw, bottom_text_position,
                            bottom, bottom_font, bottom_font_size)

    return bg_image


def _optimize_font_size(font, text, max_font_size, min_font_size,
                        max_text_len):
    """Calculate the optimal font size to fit text in a given size."""

    # Check size when using smallest single line font size
    fontobj = ImageFont.truetype(font, min_font_size)
    text_size = fontobj.getsize(text)

    # Calculate font size for text, split if necessary
    if text_size[0] > max_text_len:
        phrases = _split(text)
    else:
        phrases = (text,)
    font_size = max_font_size // len(phrases)
    for phrase in phrases:
        font_size = min(_maximize_font_size(font, phrase, max_text_len),
                        font_size)

    # Rebuild text with new lines
    text = '\n'.join(phrases)

    return font_size, text


def _draw_outlined_text(draw_image, text_position, text, font, font_size):
    """Draw white text with black outline on an image."""

    # Draw black text outlines
    outline_range = max(1, font_size // 25)
    for x in range(-outline_range, outline_range + 1):
        for y in range(-outline_range, outline_range + 1):
            pos = (text_position[0] + x, text_position[1] + y)
            draw_image.multiline_text(pos, text, (0, 0, 0),
                                      font=font, align='center')

    # Draw inner white text
    draw_image.multiline_text(text_position, text, (255, 255, 255),
                              font=font, align='center')


def _maximize_font_size(font, text, max_size):
    """Find the biggest font size that will fit."""
    font_size = max_size

    fontobj = ImageFont.truetype(font, font_size)
    text_size = fontobj.getsize(text)
    while text_size[0] > max_size and font_size > 1:
        font_size = font_size - 1
        fontobj = ImageFont.truetype(font, font_size)
        text_size = fontobj.getsize(text)

    return font_size


def _split(text):
    """Split a line of text into two similarly sized pieces.

    >>> _split("Hello, world!")
    ('Hello,', 'world!')

    >>> _split("This is a phrase that can be split.")
    ('This is a phrase', 'that can be split.')

    >>> _split("This_is_a_phrase_that_can_not_be_split.")
    ('This_is_a_phrase_that_can_not_be_split.',)

    """
    result = (text,)

    if len(text) >= 3 and ' ' in text[1:-1]:  # can split this string
        space_indices = [i for i in range(len(text)) if text[i] == ' ']
        space_proximities = [abs(i - len(text) // 2) for i in space_indices]
        for i, j in zip(space_proximities, space_indices):
            if i == min(space_proximities):
                result = (text[:j], text[j + 1:])
                break

    return result
