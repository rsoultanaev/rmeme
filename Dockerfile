FROM tiangolo/uwsgi-nginx-flask:python3.5

RUN pip3 install Pillow
RUN pip3 install requests

COPY ./app /app

# Download and set up the font file
RUN apt-get update && apt-get install unzip
RUN wget https://www.1001freefonts.com/d/5927/anton.zip -P /fonttemp && \
    unzip /fonttemp/anton.zip -d /fonttemp && \
    cp /fonttemp/Anton.ttf /app/anton.ttf && \
    rm -r /fonttemp
